"""
Project: rnqc
File: rnqc


Contact:
---------
salturki@gmail.com

21:19 
3/30/15
"""
__author__ = 'Saeed Al Turki'

import click
import pysam
from bx.intervals.intersection import Intersecter, Interval
from copy import deepcopy

gene_models = {}
transcripts = {}
junctions = {}

results = {}


def convert_chrom(chrom):
    if chrom == 23:
        chrom = 'X'
    elif chrom == 24:
        chrom = 'Y'
    return chrom


def get_gene_name(chrom, pos):
    if chrom not in transcripts:
        return None
    ts = transcripts[chrom].find(pos, pos)
    if ts is None:
        return None
    for t in ts:
        return gene_models[chrom][(t.start, t.end)]['gene_name']


def get_read_junction(read):
    r_start_place = 'u'
    r_end_place = 'u'

    if read.reference_id > 23:
        return 'x-x'

    chrom = convert_chrom(read.reference_id + 1)

    r_start = read.reference_start
    r_end = read.reference_end

    ts = transcripts[chrom].find(r_start, r_end)

    for t in ts:
        exons = gene_models[chrom][(t.start, t.end)]['exons'].find(r_start, r_start)
        if r_start in junctions[chrom]:
            r_start_place = 'j'
        elif exons:
            r_start_place = 'e'
        else:
            r_start_place = 'i'

        exons = gene_models[chrom][(t.start, t.end)]['exons'].find(r_end, r_end)
        if r_end in junctions[chrom]:
            r_end_place = 'j'
        elif exons:
            r_end_place = 'e'
        else:
            r_end_place = 'i'

    read_place = "%s-%s" % (r_start_place, r_end_place )
    if read_place == 'u-u':
        pass
    # print read_place, read.reference_id + 1, read.reference_start, read.reference_end
    return read_place


def bam_stat(input_bam, genes_counts):
    # print "BAM stats .."

    bam = pysam.AlignmentFile(input_bam)

    for read in bam.fetch(until_eof=True):

        genes_counts['global']['total'] += 1
        if read.is_unmapped:
            genes_counts['global']['unmapped'] += 1
            continue
        else:
            genes_counts['global']['mapped'] += 1

        if read.is_read1:
            genes_counts['global']['read1'] += 1
        elif read.is_read2:
            genes_counts['global']['read2'] += 1

        if read.is_duplicate:
            genes_counts['global']['duplicated'] += 1

        if read.has_tag('SA'):  # An unmapped read whose mate is mapped.
            genes_counts['global']['split'] += 1

        chrom = convert_chrom(read.reference_id + 1)
        gene_name = get_gene_name(chrom, read.reference_start)

        if chrom in transcripts:
            read_junction = get_read_junction(read)
            genes_counts['global'][read_junction] += 1
            if gene_name:
                genes_counts[gene_name][read_junction] += 1

    for typ in sorted(genes_counts['global']):
        print typ, '\t', \
            genes_counts['global'][typ],\
            "\t",\
            "%.2f%%" % (genes_counts['global'][typ] / (float(genes_counts['global']['total'] - genes_counts['global']['duplicated'])) * 100)

    for gene_name in genes_counts:
        if gene_name != 'global':
            out_line = [gene_name]
            out_line.extend([str(x) for x in [v for k,v in genes_counts[gene_name].items()]])
            print "\t".join(out_line)


def load_target_genes(path):
    holder = []
    f = open(path, 'r')
    for line in f:
        if line.startswith("#"):
            continue
        holder.append(line.strip())
    return holder


def parse_gene_model(path, target_genes_path=None):
    # print "Loading gene model .."
    genes_counts = {"global": {
        'total': 0,
        'read1': 0,
        'read2': 0,
        'mapped': 0,
        'duplicated': 0,
        'unmapped': 0,
        'split': 0,
        'x-x': 0,  # chrom > 24
        'u-u': 0}
    }
    for i in ['e', 'i', 'j']:
        for j in ['e', 'i', 'j']:
            key = "%s-%s" % (i, j)
            genes_counts['global'][key] = 0
    
    if target_genes_path:
        target_genes = load_target_genes(target_genes_path)
    else:
        target_genes = None

    f = open(path, 'r')
    for line in f:
        if line.startswith('#'):
            header = line.strip().split("\t")
            continue

        line = line.strip().replace('"', '').split("\t")

        if not line[1].startswith('NM_'):
            continue

        chrom, t_start, t_end, gene_name = line[2], int(line[4]), int(line[5]), line[12]

        if target_genes is not None:
            if gene_name not in target_genes:
                continue

        if chrom not in gene_models:
            gene_models[chrom] = {}
            transcripts[chrom] = Intersecter()

        transcripts[chrom].add_interval(Interval(t_start, t_end))
        gene_models[chrom][(t_start, t_end)] = {"transcript_id": line[3],
                                           "gene_name": gene_name,
                                           'exons': Intersecter()}
        exon_starts = [int(x) for x in line[9].split(",")[:-1]]
        exon_ends = [int(x) for x in line[10].split(",")[:-1]]

        # load exon junctions
        if chrom not in junctions:
            junctions[chrom] = {}

        # exon_ends[-1] = int(line[7])  # to avoid UTR
        exons = zip(exon_starts, exon_ends)
        for exon in exons:
            gene_models[chrom][(t_start, t_end)]['exons'].add_interval(Interval(exon[0], exon[1]))
            junctions[chrom][exon[0]] = ''
            junctions[chrom][exon[1]] = ''
            
        # create place holder
        if gene_name not in genes_counts:
            genes_counts[gene_name] = {}

        for i in ['e', 'i', 'j']:
            for j in ['e', 'i', 'j']:
                key = "%s-%s" % (i, j)
                genes_counts[gene_name][key] = 0
    f.close()
    return genes_counts


def save_results(genes_counts):
    pass


@click.group(invoke_without_command=True)
@click.option('--input-bam', '-i', default='/Users/saeed/Desktop/solid_fusion.bam', help='Input file (bam)')
@click.option('--gene-model', '-g', default='/Users/saeed/bioApps/deTango/refGene_hg19_sorted.txt',
              help='Gene model in a bed format')
@click.option('--target-genes', '-t', default='cid_solid_fusion_genes.txt', help='Text file of target gene HUGO symbols (one per line)')
def cli(input_bam, gene_model, target_genes):
    """QC for CID RNA-Seq bam files"""
    genes_counts = parse_gene_model(gene_model, target_genes)
    bam_stat(input_bam, genes_counts)


if __name__ == '__main__':
    cli()  # cli: command line interface